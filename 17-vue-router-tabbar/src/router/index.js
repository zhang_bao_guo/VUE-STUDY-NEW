import VueRouter from "vue-router";
import Vue from "vue";

const Category = () => import('@/views/category/Category')
const Home = () => import('@/views/home/Home')
const Profile = () => import('@/views/profile/Profile')
const Shopcar = () => import('@/views/shopcar/ShopCar')

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        component: Home,
        meta: {
            title: '首页'
        }
    },
    {
        path: '/profile',
        component: Profile,
        meta: {
            title: '我的'
        }
    },
    {
        path: '/shopcar',
        component: Shopcar,
        meta: {
            title: '购物车'
        }
    },
    {
        path: '/category',
        component: Category,
        meta: {
            title: '分类'
        }
    }

]
const router = new VueRouter({
    routes
})
router.beforeEach((to, from, next) => {
    document.title = to.matched[0].meta.title
    next()
})

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default router

import VueRouter from "vue-router";
import Vue from "vue";

// import Home from "../components/Home";
// import User from "../components/User";
// import About from "../components/About";

const Home = () => import("../components/Home")
const About = () => import("../components/About")
const User = () => import("../components/User")
const HomeNews = () => import('../components/HomeNews')
const HomeMsg = () => import('../components/HomeMsg')
const Profile = () => import("../components/Profile")

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/Home'
    },
    {
        path: '/Home',
        name: 'Home',
        component: Home,
        meta: {
            title: '首页'
        },
        children: [
            // {
            //     path: '/Home',
            //     redirect: '/Home/News'
            // },
            {
                // path: "/Home/News",
                // 也可以不写父路由的路径，直接写子路由的路径，注意不要加 /
                //      会自动和父路径拼接
                path: "News",
                component: HomeNews,
                meta: {
                    title: '首页-新闻'
                },
                //  组件导航守卫,也可以在组件内部调用该方法。 对应beforeRouteEnter
                // beforeEnter: (to, from, next) => {
                //     document.title = to.matched[1].meta.title,
                //         next()
                // }
            },
            {
                // path: "/Home/Msg",
                path: "Msg",
                component: HomeMsg,
                meta: {
                    title: "首页-消息"
                },
                // beforeEnter:(to,from,next)=>{
                //     next();
                //     document.title = to.matched[1].meta.title
                // }
            },
        ]
    },
    {
        path: '/About/:about',
        name: 'About',
        component: About,
        meta: {
            title: '关于'
        }
    },
    {
        path: '/User/:uid',
        name: 'User',
        component: User,
        meta: {
            title: '用户'
        }
    },
    {
        path: '/Profile',
        name: 'Profile',
        component: Profile,
        meta: {
            title: '档案'
        }
    }
]
const router = new VueRouter({
        routes,
        linkActiveClass: 'active'
    }
);
//全局导航守卫
router.beforeEach(((to, from, next) => {
    document.title = to.matched[0].meta.title
    next()
}))
export default router

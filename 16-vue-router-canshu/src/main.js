import Vue from 'vue'
import App from './App.vue'
import router from "./router/index";
Vue.config.productionTip = false

Vue.prototype.$test=function () {
  console.log("我是自定义方法");
}
Vue.prototype.$name='我是自定义name'


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})


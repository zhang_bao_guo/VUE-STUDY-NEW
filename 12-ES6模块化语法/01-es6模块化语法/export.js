const getUser = "/user/getUser/list"
// 错误写法  export getUser
export {getUser}
// 这种写法等价于
export const getDept = "/user/getDept";

//=============================

// 导出多个
const a = 1;
const b = 2;
const c = 3;
const d = 4;
export {a, b, c, d}

//=============================
// 导出对象
const student = {
    name: "小学生",
    age: 18
}
export {student}

//=============================
// 导出方法
function fun() {
    console.log("funcccccc")
}

export {fun}

//一个文件即模块中只能存在一个export default语句，导出一个当前模块的默认对外接口

// 错误写法
// export  default const store = {
//     game:"DNF",
//     moba:"LOL"
// }

// 正确写法1:
// const store = {
//     game:"DNF",
//     moba:"LOL"
// }
// export default store

// 正确写法2:
export default {
    game: "DNF",
    moba: "LOL"
}

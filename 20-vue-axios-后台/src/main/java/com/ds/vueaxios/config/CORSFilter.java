package com.ds.vueaxios.config;

import org.apache.catalina.filters.CorsFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * @program: vueaxios
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-06-06
 **/
@Configuration
public class CORSFilter extends WebMvcConfigurerAdapter {

    /**
     * Creates a new instance of CORSFilter.
     */

    public CORSFilter() {
        // TODO Auto-generated constructor stub
    }


    @Override
    public void addCorsMappings(CorsRegistry registry){
        super.addCorsMappings(registry);
        registry.addMapping("/**");//针对所有接口
        //下面这三个方法我怎么也找不到，我找了spring-webmvc的4.3.9/4.3.22/5.1.6三个版本
        //CorsRegistry里面都只有addMapping(String pathPattern)和Map<String, CorsConfiguration> getCorsConfigurations()这两个方法
        //不知道allowedMethods(),allowedOrigins(),allowedHeaders()这三个方法来自哪个版本
        //不过只调用addMapping("/**")依然可以解决跨域问题，不过如果你有一些个性化需求，比如拦截或者放过部分方法，那么则不可用。
        //registry.allowedMethods("*")
        //    .allowedOrigins("*")
        //    .allowedHeaders("*");
    }

}

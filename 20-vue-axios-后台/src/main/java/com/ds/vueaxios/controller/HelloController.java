package com.ds.vueaxios.controller;

import com.alibaba.fastjson.JSON;
import com.ds.vueaxios.entity.User;
import com.ds.vueaxios.uitl.CommonRES;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: vueaxios
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-06-06
 **/
@RestController
public class HelloController {

    @PostMapping("/user")
    public CommonRES regUser(@RequestBody User user) {
        System.out.println(user);
        return new CommonRES().getSuccess("操作成功", CommonRES.RES_SUCCESS, user);
    }

    @GetMapping("/getUser")
    public CommonRES getUser() {

        User user = User.builder().mail("qweq@qq.com").password("123123").gender("male").hobby(new String[]{"sport"}).build();
        System.out.println(user);
        return new CommonRES().getSuccess("操作成功", CommonRES.RES_SUCCESS, user);
    }

    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable("id")String id) {
        List<User> list = new ArrayList<>();
        String[] str = {"sport", "music"};
        list.add(new User("1", "123@qq.com", "admin", "male", str));
        list.add(new User("2", "123@qq.com", "admin", "male", str));
        list.add(new User("3", "123@qq.com", "admin", "male", str));
        for (User user : list) {
            if (user.getId().equals(id)){
                return user;
            }
        }
        return null;
    }

    @PostMapping("/login")
    public CommonRES login(@RequestBody String user) {
        System.out.println(user);
        User user1 = JSON.parseObject(user, User.class);
        System.out.println(user1);
        if ("admin".equals(user1.getMail()) && "admin".equals(user1.getPassword())) {
            return new CommonRES<>().getSuccess("登录成功", CommonRES.RES_SUCCESS, user);
        } else {
            return new CommonRES<>().getSuccess("登录失败", CommonRES.RES_FAIL, "");
        }
    }
}

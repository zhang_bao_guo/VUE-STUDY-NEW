package com.ds.vueaxios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VueaxiosApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueaxiosApplication.class, args);
    }

}

package com.ds.vueaxios.entity;

import lombok.*;

import java.io.Serializable;

/**
 * @program: vueaxios
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-06-06
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    private String id;
    private String mail;
    private String password;
    private String gender;
    private String [] hobby;
}

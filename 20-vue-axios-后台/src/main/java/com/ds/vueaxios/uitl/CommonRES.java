package com.ds.vueaxios.uitl;

import com.sun.org.apache.xml.internal.serializer.utils.MsgKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: vueaxios
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-06-06
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonRES<T> {
    public static String RES_FAIL = "400";
    public static String RES_SUCCESS = "200";
    private String code;
    private String msg;
    private T data;

    public CommonRES getSuccess(String msg, String code, Object data) {
        this.msg = msg;
        this.code = code;
        this.data = (T) data;
        return this;
    }

}

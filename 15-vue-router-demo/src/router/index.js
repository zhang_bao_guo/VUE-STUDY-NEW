import VueRouter from 'vue-router';
import Vue from 'vue';

import Home from '../components/Home.vue';
import About from '../components/About.vue';

Vue.use(VueRouter)


const routes = [
    {
        path: '/',
        redirect: "/Home",
    },

    {
        path: '/Home', component: Home
    },

    {
        path: '/About', component: About
    },
]

const router = new VueRouter({
    routes,
    mode: 'hash'
})



export default router

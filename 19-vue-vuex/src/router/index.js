import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter)

const Hello = ()=>import('@/components/Hello.vue')

const routes=[
    {
        path:'/hello',
        component:Hello
    }
]

let router = new VueRouter(
    {
        routes
    }
);

export default router

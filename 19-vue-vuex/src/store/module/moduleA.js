const moduleA = {
    state: {
        name: "moduleA的name"
    },
    getters: {
        fullName(state) {
            return state.name + '111'
        },
        getFullName(state, getters) {
            return getters.fullName + 'get'
        },
        //第一个参数是当前module的state，第二个参数是getters，第三个是根节点的State
        rootFullName(state, getters, rootState) {
            return getters.getFullName + rootState.count
        }
    },
    mutations: {
        //如果这个方法的名字和存在一模一样的，那么会同事执行这2个方法
        updateName(state, value) {
            state.name = value
        }
    },
    modules: {},
    actions: {
        actionUpdateName(context,value){
            setTimeout(()=>{
                console.log(context)
                context.commit('updateName',value)
            },1000)
        }
    }
}
export default moduleA

import Vue from "vue";
import Vuex from 'vuex'
import moduleA from "@/store/module/moduleA";
//1：安装插件
Vue.use(Vuex)


//2：创建对象
const store = new Vuex.Store({
    state: {
        count: 0,
        student: [
            {
                id: 100, name: '小明', age: 13
            },
            {
                id: 101, name: '小红', age: 23
            },
            {
                id: 102, name: '小白', age: 33
            },
            {
                id: 103, name: '小绿', age: 53
            },
        ],
        info: {
            id: 101,
            name: '张三',
            height: '1.89'
        }
    },
    //参考计算属性computed，第一个参数：state,第二个参数：getters，第三个：rootState
    getters: {
        more20Stu(state) {
            return state.student.filter(x => x.age > 20)
        },
        moreAgeStu(state) {
            return age => state.student.filter(s => s.age > age)
        }
    },
    //所有的同步操作写在这里面，第一个参数：state,第二个参数：传过来的参数，
    mutations: {
        addCount(state) {
            state.count++
        },
        preCount(state, value) {
            console.log(value)
            state.count--
        },
        updateInfo(state) {
            state.info.name = '小明'
            // //vuex的响应式前提：属性以前提前在store里面定义过了，追加的属性是不会触发响应式的  脚手架4.x则不会出现该情况
            // state.info['address'] = '南京'
            // //解决方法:使用Vue.set(追加属性),Vue.delete(删除某个属性)触发响应式
            // Vue.set(state.info, 'address', '鼓楼')
            // Vue.delete(state.info, 'name')


            //在mutation中使用异步操作，会使devtools不能跟踪修改后的属性，
            // setTimeout(()=>{
            //     state.info.name = '小明'
            // })

        }
    },
    //使用异步操作
    actions: {
        // context 可以理解成 store对象，第一个参数，是store对象，第二个参数是传过来的参数
        actionUpdateInfo(context, payload) {
            setTimeout(() => {
                console.log(payload)
                payload.success(payload.msg)
                context.commit('updateInfo')
            }, 1000)
        }
    },
    modules: {
        //   注册改module
        moduleA,
    },
})
export default store

import Axios from "axios";

export default function axios(config) {
    const instance = Axios.create();
    instance.interceptors.request.use(config => {
        return config
    })

    return instance(config)
}

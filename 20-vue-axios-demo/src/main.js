import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
// 使用封装的axios

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

//使用的是全局的axios
//设置默认的url
axios.defaults.baseURL = "http://localhost:8700/oa"
//axios 基本使用
// axios({
//     method: 'get',
//     url: '/getUser',
//     timeout: 1000
// })
//     .then(res => console.log(res))
//     .catch(() => {
//         alert('请求超时')
//     })
// axios.get('/user/1').then(res => {
//     this.$data.user = res.data
// })
// axios.post('/login', {
//     mail:'admin',
//     password:'admin'
// }).then((res=>{
//     alert(res.data.msg)
// }))

//发送并发请求
// axios.all([
//     axios(
//         {
//             method: 'get',
//             url: '/getUser'
//         }),
//     axios.get('/user/1')
// ])
//     //  默认把2个请求的返回值放在一个数组里面
//     // .then(res=>{console.log(res)})
//     // Axios.spread 把返回的数据，按照res1，res2来展开，有多少个请求，数据的长度就是多少
//     .then(axios.spread((res1, res2) => {
//         console.log(res1)
//         console.log(res2)
//     }))


//---------------------------------------

//创建axios实例
// const instance = axios.create({
//     baseURL: 'http://localhost:8080',
//     timeout: 2000
// })
// instance.get('/user/1').then(res=>console.log(res))
// instance.post('/login',{
//   mail:'admin',
//   password:'admin',
//   id:'123',
//   gender:'312',
//   hobby:['sport','music']
// }).then(res=>{
//   console.log(res);
// })

//---------------------------------------

// request(
//     {
//         method:'get',
//         url: '/user/1'
//     },
//     res => {
//         console.log(res)
//     },
//     err=>{
//         console.log(err)
//     })


// request({
//     method: 'post',
//     url: '/login',
//     data: {
//         mail: 'admin',
//         password: 'admin'
//     },
// }).then(res => console.log(res))
//     .catch(err => console.log(err))
